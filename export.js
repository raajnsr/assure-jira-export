const JiraApi = require('jira-client');
const utilities = require('./lib');

// Get the required input from user
const credentials = {};
let jira , config, projKey, filesCount, dirPath;
let createdIDs = "";
let jiraIssues = [];

setCredentials();

// Retrieve configuration from config file and establish the connection
function setCredentials(){
   console.log("\r\nSetting up the configuration... Please wait...");
   config = require('./config.json');

  // Initialize and login to Jira
  jira =  new JiraApi({
    protocol: 'https',
    host: config.credentials.host,
    username:  config.credentials.userName ,
    password:  config.credentials.password,
    apiVersion: '2',
    strictSSL: true
  });

  // Set the jira instance
  utilities.setJiraInstance(jira);

  utilities.validateCredentials()
  .then(function(result){
    // successfully connected
    projKey = config.projectKey;
    dirPath = config.directoryPath;
    if(!dirPath.trim().endsWith("/")){
      dirPath += "/";
    }
    createCustomField();
  })
  .catch(function(err){
    console.log("Couldn't establish the JIRA connection, please check the credentials\r\n");
    process.exit();
  });
};

// Retrieve projects from the JIRA instance
getProjects => {
  const projKey = "";
  var wsAssureField;
  // Get projects
  utilities.getProjectsList(projectsCallback);
}

// Callback to be called once the projects are fetched - for MVP, not using this method
function projectsCallback(projects){
  var projs = "Available projects: ";
  var keys = [];
  projects.forEach(function(project){
    projs += project.name + "(key: " + project.key + "),";
    keys.push(project.key);
  });
  projs = projs.substr(0,projs.length-1);
  console.log(projs);
  promptly.prompt("Please choose the JIRA project to export issues (enter project key): ")
  .then((key) => {
    if (keys.indexOf(key) != -1){
        projKey = key;
        console.log("\r");
        createCustomField();
    } else {
      console.log("Please enter a valid project key \r");
      projectsCallback(projects);
    }
  });
};

function createCustomField(){
  // Create a custom field to store Assure accessibility issues data
  const assureField = {
        "name": "WS Assure A11Y Issues",
        "description": "Custom field for holding WorldSpace Assure Accessibility Issues ",
        "type": "com.atlassian.jira.plugin.system.customfieldtypes:textarea"
   };
  utilities.createCustomField(assureField, customFieldCallback);
};

function customFieldCallback(field){
  wsAssureField = field;
  // Associate created custom field with respective screens
  utilities.getProjectScreens(projKey, screensCallback);
 };

 function screensCallback(screens){
    let totalScreens = screens.length;
    let screenInd = 0;
    let lastScreen = false;

    screens.forEach(function(screen){
      screenInd = screenInd + 1;
      if (totalScreens == screenInd){
         lastScreen = true;
      }
      utilities.getTabs(lastScreen, projKey,screen,tabsCallback);
    });
 };

 function tabsCallback(lastScreen, screen, tabs){
  var totalTabs = tabs.length;
  var tabInd = 0;
  var lastTab= false;
    tabs.forEach(function(tab){
      tabInd = tabInd + 1;
      if (totalTabs == tabInd){
         lastTab = true;
      };

      for(key in tab) {
        if(tab.hasOwnProperty(key)) {
          const value = tab[key];
            utilities.addFieldToScreenTab(lastScreen, lastTab, screen, value.id, wsAssureField, startExportProcess);
        };
      };
    });
 };

 // Starts export process
 function startExportProcess(){
   try{
      processAssureDataDir();
   }
   catch(err){
      console.log("Error during processing the files, error: " + err +"\r\n");
      process.exit();
   }
 };

 // Process directory of files
 function processAssureDataDir(){
  try {
     console.log("Reading .csv and .json files from directory '"+ dirPath + "' and exporting issues to JIRA...\r\n");
      utilities.processDirectory(config)
      .then (function(files){
          filesCount = files.length;
          if (filesCount == 0){
            console.log("No files to process, please check the directory...\r\n");
            process.exit();
          }
          else {
            processAssureDataFiles(files);
          }
       });
    }
  catch(err){
     throw err;
    }
 };

 // Process files
 function processAssureDataFiles(files){
  let jiraIssues = [];
  let timer;
    try{
      new Promise(function(resolve, reject){

        files.forEach(function(file){
          const filepath = dirPath + file;
          if(file.endsWith('.json')){
              jiraIssues.push(getJiraIssueForJSONObject(filepath));
          };
          if (file.endsWith('.csv')){
              utilities.convertCSVtoJSON(filepath)
              .then(function(wsIssues){
                  jiraIssues.push(getJiraIssueForCSVObject(wsIssues));
              });
          };
        });

        //need to revisit this piece of code - not sure how to do other than this way
        timer = setInterval(function(){
            if(jiraIssues.length == files.length){
              clearInterval(timer);
              resolve();
              if (jiraIssues.length == 0){
                 console.log("No records found in the submitted files and hence no issues exported to jira... \r\n");
                 process.exit();
              } else {
                exportToJira(jiraIssues);;
              }
            }
         }, 1000);
      });
    }
    catch(err){
      reject(err);
    };
 };

 // Get JSON object from CSV row aligning JIRA schema
 function getJiraIssueForCSVObject(assureIssues)
 {
   try{
    let issueIDs = "";
    let groupedIssues = "";

    assureIssues.forEach(function(memb){
      issueIDs += memb['Issue ID'] + " ";
      groupedIssues += "h2. Issue " + memb['Issue ID'] + "\r";
      groupedIssues += "{panel:title="+ memb['Summary'] + "}";
      groupedIssues += "*Description:* "+ memb['Description'] + "\r\r";
      groupedIssues += "*Impact:* "+ memb['Impact'] + "\r\r";
      groupedIssues += "*Recommendation:* " + memb['Recommended to fix'] + "\r\r";
      groupedIssues += "*Source code:* " + memb['Source Code'] + "\r\r";
      groupedIssues += "*WCAG checkpoint:* " + memb['Checkpoint'] + "\r\r";
      groupedIssues += "*Unit type:* " + memb['Unit Type'] + "\r\r";
      groupedIssues += "*Unit:* " + memb['Test Unit'] + "\r\r";
      groupedIssues += "*Page name:* " + memb['Test Case Name'] + "\r\r";
      groupedIssues += "*URL of the page:* " + memb['URL'] + "\r\r";
      groupedIssues += "*Help URL:* " + memb['More Info'] + "\r\r";
      groupedIssues += "*Standards:* " + memb['Standards'] + "\r\r";
      groupedIssues += "{panel}\r\r";
    });
    var jIssue = {
        "fields":
             { "project": { "key": projKey},
               "summary": "WorldSpace Assure Accessibility Issues " + issueIDs,
               [wsAssureField.id]: groupedIssues,
               "issuetype" : { "name" : "Bug"}
           }
         };
    }
    catch(err){
        throw err;
    }
     return jIssue;
 };

// Get JSON object aligning JIRA schema
function getJiraIssueForJSONObject(path){
   try {
    //parse assure data
    let assureIssues = require(path);
    let issueIDs = "";
    let groupedIssues = "";

    assureIssues.forEach(function(memb){
      issueIDs += memb.ordinal + " ";
      groupedIssues += "h2. Issue " + memb.ordinal + "\r";
      groupedIssues += "{panel:title="+ memb.summary + "}";
      groupedIssues += "*Description:* "+ memb.description + "\r\r";
      groupedIssues += "*Impact:* "+ memb.impact + "\r\r";
      groupedIssues += "*Recommendation:* " + memb.recommendedToFix + "\r\r";
      groupedIssues += "*Source code:* " + memb.source + "\r\r";
      groupedIssues += "*WCAG checkpoint:* " + memb.checkpoint + "\r\r";
      groupedIssues += "*Unit type:* " + memb.unit_type + "\r\r";
      groupedIssues += "*Unit:* " + memb.unit + "\r\r";
      groupedIssues += "*Page name:* " + memb.name + "\r\r";
      groupedIssues += "*URL of the page:* " + memb.url + "\r\r";
      groupedIssues += "*Help URL:* " + memb.help_url + "\r\r";
      groupedIssues += "*Standards:* " + memb.standards + "\r\r"
      groupedIssues += "{panel}\r\r";
    });
      var jIssue = {
        "fields":
             { "project": { "key": projKey},
               "summary": "WorldSpace Assure Accessibility Issues " + issueIDs,
               [wsAssureField.id]: groupedIssues,
               "issuetype" : { "name" : "Bug"}
           }
         };
    }
    catch(err){
      throw err;
    }
    return jIssue;
 };

 // Export issues to JIRA
 function exportToJira(jiraIssues){
    var ind = 0;
    for (let i in jiraIssues){
     jira.addNewIssue(jiraIssues[i])
    .then(function(issue) {
        ind = ind + 1;
        if (ind == jiraIssues.length){
          createdIDs += issue.key;
          console.log("\rTotally processed " + jiraIssues.length + " files...");
          console.log("\rCreated JIRA issues (keys): " + createdIDs);
          closeProcess();
        }
        else {
            createdIDs += issue.key + " , ";
        }
      })
      .catch(function(err) {
        console.error("Error while adding issues to JIRA, error : " + err);
        process.exit();
      });
    };
};

 function closeProcess(){
  console.log("\r\nExporting Assure a11y issues to JIRA instance is completed.\r\n");
  process.exit();
 };