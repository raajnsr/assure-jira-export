# Assure_JIRA_Export

Steps to install and execute this code

# Pre-requisites:

1. Have JIRA instance and credentials, with admin previliges
2. Make sure JIRA instance has some projects configured on it
3. Install node.js - https://nodejs.org/en/download/ 
4. Have the Assure data to export in the form of either .json or .csv

# Installation

1. Get the code from this repository
2. On node command prompt navigate to project directory and run 'npm install' to get required dependencies from package.json

Edit config file and provide right details

 1. host url
 2. user name
 3. password
 4. directory path where Assure exported .csv/.json files exist

Open the command prompt or terminal
Change the directory to be the path of source code
Run the command 'node export.js' in the command line
Window should give you success or failure message of the exporting task

# additional configuration steps - one time effort per JIRA instance

When you run this program for first time (only for one time), go to JIRA instance and configure the custom field to use wiki style renderer

To do this, Navigate JIRA -> Projects -> Settings -> Issues -> Field Configurations -> Default field configuration -> 
search for custom field “WS Assure A11Y Issues” and click “Renderers” link against it.

Modify the option to be “Wiki Style Renderer”

# additional precautions

This is tested against "Kanban" and "Bug tracking" JIRA boards. 

Put only valid .csv or .json files in the directory. Do not to keep any other INVALID .csv or .json files in the folder as the code will not  create JIRA tickets if it gets an exception/erorr during the process, due to any of the invalid file(s). I can fix the code for it but do not want to create more console messages with status pass/fail for demo purposes. 

Do not try to export too many records as Summary and custom field that we created for this export feature do have certain limits in terms of number of characters. So for demo purposes, try with less number of records and files. We will address all these limitations when we make it as product.

